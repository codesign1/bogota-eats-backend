'use strict';

module.exports = function(Comentarios) {

  Comentarios.comentariosRestaurante= function (id_restaurante, cb) {
    var filter = {
      where:{
        id_restaurante:id_restaurante
      }
    };
    Comentarios.find("", function (err, instance) {
      // body...
      var response = instance;
      var response = [];
      console.log(instance);
      instance.forEach(function (comment) {
        if(id_restaurante == comment.id_restaurante){
          response.push(comment);
        }
      });
      cb(null, response);
    });
  };

  Comentarios.remoteMethod(
    'comentariosRestaurante',
    {
      http: {
        path: '/comentariosRestaurante',
        verb: 'get'
      },
      description:'Comentarios del restaurante',
      accepts: [
        {
          arg: 'id_restaurante',
          type: 'string',
          description: 'Id del restaurante deseado'
        }
      ],
      returns: [
        {
          arg: 'comentarios',
          type: 'array'
        }
      ]
    }
  );

};

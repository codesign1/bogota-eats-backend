'use strict';

module.exports = function(Banner) {

  Banner.BannerActual= function (fecha, cb) {
    var filter = {
      where:{
        fecha_inicio:{lt: new Date()},
        fecha_fin:{gt: new Date()}
      }
    };
    Banner.find(filter, function (err, instance) {
      // body...
      var response = instance;
      cb(null, response);
    });
  };

  Banner.remoteMethod(
    'BannerActual',
    {
      http: {
        path: '/BannerActual',
        verb: 'get'
      },
      description:'Banners activos',
      accepts: [
        {
          arg: 'fecha',
          type: 'date',
          description: 'Fecha actual'
        }
      ],
      returns: [
        {
          arg: 'Banner',
          type: 'array'
        }
      ]
    }
  );
};

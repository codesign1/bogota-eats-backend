'use strict';

module.exports = function(Restaurantes) {

  Restaurantes.newsfeed = function (restaurante, cb) {
    var filter = {where: {estado: "publicada"}} ;
    Restaurantes.find(filter, function (err, instance) {
      // body...
      instance.forEach(function (rest) {
        rest.fotos = [rest.fotos[0]];
        rest.comentario = "";
        rest.direccion = "";
      })
      var response = instance;
      cb(null, response);
    });
  };

  Restaurantes.remoteMethod(
    'newsfeed',
    {
      http: {
        path: '/newsfeed',
        verb: 'get'
      },
      description:'newsfeed',
      accepts: [
        {
          arg: 'restaurante',
          type: 'string',
          description: 'restaurante'
        }
      ],
      returns: [
        {
          arg: 'news',
          type: 'array'
        }
      ]
    }
  );

};

(function () {

  'use strict';

  angular
    .module('app', ['auth0.lock', 'angular-jwt', 'ui.router', 'ngFileUpload'])
    .config(config);

  config.$inject = ['$stateProvider', 'lockProvider', '$urlRouterProvider'];

  function config($stateProvider, lockProvider, $urlRouterProvider) {

    $stateProvider
      .state('home', {
        url: '/home',
        controller: 'HomeController',
        templateUrl: '../assets/js/components/home/home.html',
        controllerAs: 'vm'
      })
      .state('login', {
        url: '/login',
        controller: 'LoginController',
        templateUrl: '../assets/js/components/login/dash.html',
        controllerAs: 'vm'
      })
      .state('dash', {
        url: '/dash',
        controller: 'DashController',
        templateUrl: '../assets/js/components/dashboard/dash.html',
        controllerAs: 'vm'
      })
      .state('new', {
        url: '/new/:id',
        controller: 'NewController',
        templateUrl: '../assets/js/components/new/new.html',
        controllerAs: 'vm'
      })
      .state('banners', {
        url: '/banners',
        controller: 'BannerController',
        templateUrl: '../assets/js/components/banners/banner.html',
        controllerAs: 'vm'
      })
      .state('terminos', {
        url: '/terminos',
        controller: 'TerminosController',
        templateUrl: '../assets/js/components/terminos/terminos.html',
        controllerAs: 'vm'
      });

    lockProvider.init({
      clientID: AUTH0_CLIENT_ID,
      domain: AUTH0_DOMAIN
    });

    $urlRouterProvider.otherwise('/home');
  }

})();

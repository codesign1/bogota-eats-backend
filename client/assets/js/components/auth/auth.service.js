(function () {

  'use strict';

  angular
    .module('app')
    .service('authService', authService);

  authService.$inject = ['lock', 'authManager', '$state'];

  function authService(lock, authManager, $state) {

    function login() {
      lock.show();
    }

    // Logging out just requires removing the user's
    // id_token and profile
    function logout() {
      sessionStorage.removeItem('id_token');
      sessionStorage.removeItem('profile');
      $state.go('home');
      authManager.unauthenticate();
    }

    // Set up the logic for when a user authenticates
    // This method is called from app.run.js
    function registerAuthenticationListener() {
      lock.on('authenticated', function (authResult, profile) {
        lock.getProfile(authResult.idToken, function(error, profile) {
            if (error) {
              // Handle error
              return;
            }
            if(profile.user_metadata.roles[0]== 'admin') {
              sessionStorage.setItem('idToken', authResult.idToken);
              sessionStorage.setItem('profile', JSON.stringify(profile));
              $state.go('dash');
            }
          });

        authManager.authenticate();
      });
    }

    return {
      login: login,
      logout: logout,
      registerAuthenticationListener: registerAuthenticationListener
    }
  }
})();

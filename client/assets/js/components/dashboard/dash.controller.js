(function () {
  'use strict';

  angular
    .module('app')
    .controller('DashController', DashController);

  DashController.$inject = ['authService', '$location', '$http', '$scope'];

  function DashController(authService, $location, $http, $scope) {

    $('.collapsible').collapsible();

    var vm = this;
    vm.authService = authService;
    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        vm.authService.logout();
        $state.go('home');
      }
    }
    else{
      vm.authService.logout();
      $state.go('home');
    }

    function load() {
      $http.get("api/restaurantes").then(function (res) {
        vm.restaurantes = res.data;
        $scope.restaurantes = res.data;
        console.log("restaurantes", res);
      });
    }

    load();

    vm.publicarResena = function (id) {
      $http.put("api/restaurantes/"+id, {estado: 'publicada'}).then(function (res) {
        load();
      });
    };

    vm.guardarReseña = function (id) {
      $http.put("api/restaurantes/"+id, {estado: 'guardada'}).then(function (res) {
        load();
      });
    };

    vm.ocultarResena = function (id) {
      $http.put("api/restaurantes/"+id, {estado: 'oculta'}).then(function (res) {
        load();
      });
    };

    vm.eliminar = function (id) {
      $http.delete("api/restaurantes/"+id).then(function (res) {
        load();
      });
    };

  }
}());

(function () {
  'use strict';

  angular
    .module('app')
    .controller('TerminosController', TerminosController);

  TerminosController.$inject = ['authService', '$location', '$stateParams', '$http', '$scope', '$state'];

  function TerminosController(authService, $location, $stateParams, $http, $scope, $state) {


    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        authService.logout();
        $state.go('home');
      }
    }
    else{
      authService.logout();
      $state.go('home');
    }

    function loadTerminos() {
      var source = "/api/terminos";
      $http.get(source).then(function (res) {
        $scope.termino = res.data[0];
      })
    }

    loadTerminos();

    $scope.updateTerminos = function(termino) {
      var source = "/api/terminos";
      $http.put(source, termino).then(function (res) {
        $state.go('dash');
      });
    }

  }

}());

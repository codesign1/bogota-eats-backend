(function () {

  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['authService', '$state'];

  function HomeController(authService, $state) {

    var vm = this;
    vm.authService = authService;
    if(sessionStorage.getItem('profile')){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo == 'admin'){
        $state.go('dash');
      }
    }
  }

}());

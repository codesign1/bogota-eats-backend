(function () {
  'use strict';

  angular
    .module('app')
    .controller('NewController', NewController);

  NewController.$inject = ['authService', '$location', '$stateParams', '$http', '$scope', '$state'];

  function NewController(authService, $location, $stateParams, $http, $scope, $state) {


    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        authService.logout();
        $state.go('home');
      }
    }
    else{
      authService.logout();
      $state.go('home');
    }

    var date = new Date();

    date.getTime()

    $('.collapsible').collapsible();
    $('#mydiv').hide();
    var vm = this;
    vm.restaurante = {fotos:[], platos:[]};
    vm.authService = authService;
    vm.editando = false;
    if($stateParams.id){
      vm.editando = true;
      $http.get("api/restaurantes/"+$stateParams.id).then(function (res) {
        vm.restaurante = res.data;
        _.each(vm.tiposCom, function(com){
          _.each(vm.restaurante.tipoComida, function (tipoCom) {
            if(com.nombre == tipoCom){
              com.selected = true;
            }
          })
        });
        _.each(vm.tiposOca, function(oca){
          _.each(vm.restaurante.tipoOcasion, function (tipoOca) {
            if(oca.nombre == tipoOca){
              oca.selected = true;
            }
          })
        });
      });
    }
    vm.position = {};
    var userLatLng;
    var myOptions = {
      zoom : 16,
      center : userLatLng,
      mapTypeId : google.maps.MapTypeId.ROADMAP
    };


    var mapObject;

    navigator.geolocation.getCurrentPosition(locationSuccess, locationFail);


    function locationFail() {
      alert("Oops, could not find you.");
    }
    var rImg = "../../../../img/BE_Comidas";
    var ocaImg = "../../../../img/BE_Ocaciones";

    vm.tiposCom = [
      {nombre: "Americana", img: rImg+"/BE_ComidasBlack-01.png", imgSel: rImg+"/BE_ComidasWhite-01.png", selected: false},
      {nombre: "Asiatica", img: rImg+"/BE_ComidasBlack-02.png", imgSel: rImg+"/BE_ComidasWhite-02.png", selected: false},
      {nombre: "Cafe", img: rImg+"/BE_ComidasBlack-03.png", imgSel: rImg+"/BE_ComidasWhite-03.png", selected: false},
      {nombre: "Carne", img: rImg+"/BE_ComidasBlack-04.png", imgSel: rImg+"/BE_ComidasWhite-04.png", selected: false},
      {nombre: "Rapida", img: rImg+"/BE_ComidasBlack-05.png", imgSel: rImg+"/BE_ComidasWhite-05.png", selected: false},
      {nombre: "Española", img: rImg+"/BE_ComidasBlack-06.png", imgSel: rImg+"/BE_ComidasWhite-06.png", selected: false},
      {nombre: "Francesa", img: rImg+"/BE_ComidasBlack-07.png", imgSel: rImg+"/BE_ComidasWhite-07.png", selected: false},
      {nombre: "Hamburguesas", img: rImg+"/BE_ComidasBlack-08.png", imgSel: rImg+"/BE_ComidasWhite-08.png", selected: false},
      {nombre: "Italiana", img: rImg+"/BE_ComidasBlack-09.png", imgSel: rImg+"/BE_ComidasWhite-09.png", selected: false},
      {nombre: "Mediterranea", img: rImg+"/BE_ComidasBlack-10.png", imgSel: rImg+"/BE_ComidasWhite-10.png", selected: false},
      {nombre: "Mexicana", img: rImg+"/BE_ComidasBlack-11.png", imgSel: rImg+"/BE_ComidasWhite-11.png", selected: false},
      {nombre: "Peruana", img: rImg+"/BE_ComidasBlack-12.png", imgSel: rImg+"/BE_ComidasWhite-12.png", selected: false},
      {nombre: "Pizza", img: rImg+"/BE_ComidasBlack-13.png", imgSel: rImg+"/BE_ComidasWhite-13.png", selected: false},
      {nombre: "Saludable", img: rImg+"/BE_ComidasBlack-14.png", imgSel: rImg+"/BE_ComidasWhite-14.png", selected: false},
      {nombre: "Sanduche", img: rImg+"/BE_ComidasBlack-15.png", imgSel: rImg+"/BE_ComidasWhite-15.png", selected: false},
      {nombre: "Sushi", img: rImg+"/BE_ComidasBlack-16.png", imgSel: rImg+"/BE_ComidasWhite-16.png", selected: false},
      {nombre: "No Convencional", img: rImg+"/BE_ComidasBlack-17.png", imgSel: rImg+"/BE_ComidasWhite-17.png", selected: false},
      {nombre: "Fusión", img: rImg+"/BE_ComidasBlack-18.png", imgSel: rImg+"/BE_ComidasWhite-18.png", selected: false},
      {nombre: "Mar", img: rImg+"/BE_ComidasBlack-19.png", imgSel: rImg+"/BE_ComidasWhite-19.png", selected: false},
      {nombre: "Colombiana", img: rImg+"/BE_ComidasBlack-20.png", imgSel: rImg+"/BE_ComidasWhite-20.png", selected: false}
    ];

    vm.tiposOca = [
      {nombre: "Alta cocina", img: ocaImg+"/BE_Ocaciones_Black-01.png", imgSel: ocaImg+"/BE_Ocaciones_White-01.png", selected: false},
      {nombre: "Buena Terraza", img: ocaImg+"/BE_Ocaciones_Black-02.png", imgSel: ocaImg+"/BE_Ocaciones_White-02.png",  selected: false},
      {nombre: "En los rines", img: ocaImg+"/BE_Ocaciones_Black-03.png", imgSel: ocaImg+"/BE_Ocaciones_White-03.png",  selected: false},
      {nombre: "Desayunos", img: ocaImg+"/BE_Ocaciones_Black-04.png", imgSel: ocaImg+"/BE_Ocaciones_White-04.png",  selected: false},
      {nombre: "Cumpleaños", img: ocaImg+"/BE_Ocaciones_Black-05.png", imgSel: ocaImg+"/BE_Ocaciones_White-05.png",  selected: false},
      {nombre: "Domingo Abuela", img: ocaImg+"/BE_Ocaciones_Black-06.png", imgSel: ocaImg+"/BE_Ocaciones_White-06.png",  selected: false},
      {nombre: "De mes", img: ocaImg+"/BE_Ocaciones_Black-07.png", imgSel: ocaImg+"/BE_Ocaciones_White-07.png",  selected: false},
      {nombre: "Para el gringo", img: ocaImg+"/BE_Ocaciones_Black-08.png", imgSel: ocaImg+"/BE_Ocaciones_White-08.png",  selected: false},
      {nombre: "Primera cita", img: ocaImg+"/BE_Ocaciones_Black-09.png", imgSel: ocaImg+"/BE_Ocaciones_White-09.png",  selected: false},
      {nombre: "Rapida", img: ocaImg+"/BE_Ocaciones_Black-10.png", imgSel: ocaImg+"/BE_Ocaciones_White-10.png",  selected: false},
      {nombre: "Grado", img: ocaImg+"/BE_Ocaciones_Black-11.png", imgSel: ocaImg+"/BE_Ocaciones_White-11.png",  selected: false},
      {nombre: "Clandestino", img: ocaImg+"/BE_Ocaciones_Black-12.png", imgSel: ocaImg+"/BE_Ocaciones_White-12.png",  selected: false},
      {nombre: "Tarjeta corporativa", img: ocaImg+"/BE_Ocaciones_Black-13.png", imgSel: ocaImg+"/BE_Ocaciones_White-13.png",  selected: false},
      {nombre: "Trago y picadas", img: ocaImg+"/BE_Ocaciones_Black-14.png", imgSel: ocaImg+"/BE_Ocaciones_White-14.png",  selected: false},
      {nombre: "Precopeo", img: ocaImg+"/BE_Ocaciones_Black-15.png", imgSel: ocaImg+"/BE_Ocaciones_White-15.png",  selected: false},
      {nombre: "Oficinero", img: ocaImg+"/BE_Ocaciones_Black-16.png", imgSel: ocaImg+"/BE_Ocaciones_White-16.png",  selected: false}
    ];

    function locationSuccess(position) {
      userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      myOptions.center = userLatLng;
      mapObject = new google.maps.Map(document.getElementById("map"), myOptions);
      google.maps.event.addListener(mapObject, 'click', function(event) {
        addMarker(event.latLng, mapObject);
      });
      if($stateParams.id){
        addInitialMarker(vm.restaurante.geoloc, mapObject);
      }
    }
    var marker;
    function addMarker(location, map) {
      vm.restaurante.geoloc = {lat: location.lat(), lng: location.lng()};
      if ( marker ) {
        marker.setPosition(location);
      } else {
        marker = new google.maps.Marker({
          position: location,
          map: map
        });
      }
    }

    function addInitialMarker(geoloc, map) {
      console.log("set");
      var latlng = new google.maps.LatLng(geoloc.lat, geoloc.lng);
      marker = new google.maps.Marker({
        position: latlng,
        map: map
      });
    }

    vm.upload = function(file){
      console.log(file);
      var reader = new window.FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        var base64data = reader.result;
        vm.restaurante.fotos.push(base64data);
      }
    };

    vm.guardar = function (restaurante) {
      $('#mydiv').show();
      restaurante.estado = 'guardada';
      restaurante.fecha = new Date();
      restaurante.calUsers = [];
      var tiposOc = _.map(_.groupBy(vm.tiposOca, 'selected').true, function (oca) {
        return oca.nombre;
      });
      var tiposCo = _.map(_.groupBy(vm.tiposCom, 'selected').true, function (com) {
        return com.nombre;
      });
      restaurante.tipoComida = tiposCo;
      restaurante.tipoOcasion = tiposOc;
      $http.post("api/restaurantes", restaurante).then(function (res) {
        $state.go('dash');
        $('#mydiv').hide();
      }).catch(function (err) {
        var mensaje = "Favor completar todos lo campos: ";
        if(restaurante.nombre == null){
          mensaje = mensaje + " nombre";
        }
        if(restaurante.zona == null){
          mensaje = mensaje + " zona";
        }
        if(restaurante.geoloc == null){
          mensaje = mensaje + " ubicación";
        }
        if(restaurante.tipoComida.length == 0){
          mensaje = mensaje + " tipo de comida";
        }
        if(restaurante.tipoOcasion.length == 0){
          mensaje = mensaje + " tipo de ocasion";
        }
        $('#mydiv').hide();
        alert(mensaje);
      })
    };

    vm.publicar = function (restaurante) {
      $('#mydiv').show();
      restaurante.estado = 'publicada';
      restaurante.fecha = new Date();
      restaurante.calUsers = [];
      var tiposOc = _.map(_.groupBy(vm.tiposOca, 'selected').true, function (oca) {
        return oca.nombre;
      });
      var tiposCo = _.map(_.groupBy(vm.tiposCom, 'selected').true, function (com) {
        return com.nombre;
      });
      restaurante.tipoComida = tiposCo;
      restaurante.tipoOcasion = tiposOc;
      $http.post("api/restaurantes", restaurante).then(function (res) {
        $state.go('dash');
        $('#mydiv').hide();
      }).catch(function (err) {
        var mensaje = "Favor completar todos lo campos: ";
        if(restaurante.nombre == null){
          mensaje = mensaje + " nombre";
        }
        if(restaurante.zona == null){
          mensaje = mensaje + " zona";
        }
        if(restaurante.geoloc == null){
          mensaje = mensaje + " ubicación";
        }
        if(restaurante.tipoComida.length == 0){
          mensaje = mensaje + " tipo de comida";
        }
        if(restaurante.tipoOcasion.length == 0){
          mensaje = mensaje + " tipo de ocasion";
        }
        $('#mydiv').hide();
        alert(mensaje);
      })
    };

    vm.publicarNuevo = function (restaurante) {
      $('#mydiv').show();
      restaurante.fecha = new Date();
      var tiposOc = _.map(_.groupBy(vm.tiposOca, 'selected').true, function (oca) {
        return oca.nombre;
      });
      var tiposCo = _.map(_.groupBy(vm.tiposCom, 'selected').true, function (com) {
        return com.nombre;
      });
      restaurante.tipoComida = tiposCo;
      restaurante.tipoOcasion = tiposOc;
      $http.put("api/restaurantes", restaurante).then(function (res) {
        $state.go('dash');
        $('#mydiv').hide();
      }).catch(function (err) {
        var mensaje = "Favor completar todos lo campos: ";
        if(restaurante.nombre == null){
          mensaje = mensaje + " nombre";
        }
        if(restaurante.zona == null){
          mensaje = mensaje + " zona";
        }
        if(restaurante.geoloc == null){
          mensaje = mensaje + " ubicación";
        }
        if(restaurante.tipoComida.length == 0){
          mensaje = mensaje + " tipo de comida";
        }
        if(restaurante.tipoOcasion.length == 0){
          mensaje = mensaje + " tipo de ocasion";
        }
        $('#mydiv').hide();
        console.log(err);
        alert(mensaje + err);
      })
    }

    vm.removeFoto = function(index){
      vm.restaurante.fotos.splice(index, 1);
    }

  }

}());

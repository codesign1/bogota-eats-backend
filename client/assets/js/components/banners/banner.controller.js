(function () {

  'use strict';

  angular
    .module('app')
    .controller('BannerController', BannerController);

  BannerController.$inject = ['authService', '$state', '$http', '$scope'];

  function BannerController(authService, $state, $http, $scope) {

    $('.modal').modal(
      {
        dismissible: false

      }
    );
    $('.collapsible').collapsible();
    $('select').material_select();

    var vm = this;
    vm.authService = authService;
    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        vm.authService.logout();
        $state.go('home');
      }
    }
    else{
      vm.authService.logout();
      $state.go('home');
    }

    function load() {
      $http.get("api/banners").then(function (res) {
        vm.banners = res.data;
        $scope.banners = res.data;
      });
    }
    load();

    $scope.banner = {};

    vm.upload = function(file){
      var reader = new window.FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        var base64data = reader.result;
        $scope.banner.foto = base64data;
      }
    };

    vm.eliminar = function(id){
      $http.delete("api/banners/"+id);
      load();
    };

    vm.nuevoBanner = function(banner){
      banner.id_banner = 0;
      $http.post("api/banners", banner).then(function (res) {
        load();
        $scope.banner = {};
      });
    };

  }

}());

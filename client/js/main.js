(function () {

  'use strict';

  angular
    .module('app', ['auth0.lock', 'angular-jwt', 'ui.router', 'ngFileUpload'])
    .config(config);

  config.$inject = ['$stateProvider', 'lockProvider', '$urlRouterProvider'];

  function config($stateProvider, lockProvider, $urlRouterProvider) {

    $stateProvider
      .state('home', {
        url: '/home',
        controller: 'HomeController',
        templateUrl: '../assets/js/components/home/home.html',
        controllerAs: 'vm'
      })
      .state('login', {
        url: '/login',
        controller: 'LoginController',
        templateUrl: '../assets/js/components/login/dash.html',
        controllerAs: 'vm'
      })
      .state('dash', {
        url: '/dash',
        controller: 'DashController',
        templateUrl: '../assets/js/components/dashboard/dash.html',
        controllerAs: 'vm'
      })
      .state('new', {
        url: '/new/:id',
        controller: 'NewController',
        templateUrl: '../assets/js/components/new/new.html',
        controllerAs: 'vm'
      })
      .state('banners', {
        url: '/banners',
        controller: 'BannerController',
        templateUrl: '../assets/js/components/banners/banner.html',
        controllerAs: 'vm'
      })
      .state('terminos', {
        url: '/terminos',
        controller: 'TerminosController',
        templateUrl: '../assets/js/components/terminos/terminos.html',
        controllerAs: 'vm'
      });

    lockProvider.init({
      clientID: AUTH0_CLIENT_ID,
      domain: AUTH0_DOMAIN
    });

    $urlRouterProvider.otherwise('/home');
  }

})();

(function () {

  'use strict';

  angular
    .module('app')
    .run(run);

  run.$inject = ['$rootScope', 'authService', 'lock'];

  function run($rootScope, authService, lock) {
    // Put the authService on $rootScope so its methods
    // can be accessed from the nav bar
    $rootScope.authService = authService;

    // Register the authentication listener that is
    // set up in auth.service.js
    authService.registerAuthenticationListener();

    // Register the synchronous hash parser
    lock.interceptHash();
  }

})();

var AUTH0_CLIENT_ID='R1hg70wpo2mtJqaYmNITvd6J2IKQ6W3O';
var AUTH0_DOMAIN='bogotaeats.auth0.com';
var AUTH0_CALLBACK_URL='YOUR_CALLBACK_URL';

(function () {

  'use strict';

  angular
    .module('app')
    .service('authService', authService);

  authService.$inject = ['lock', 'authManager', '$state'];

  function authService(lock, authManager, $state) {

    function login() {
      lock.show();
    }

    // Logging out just requires removing the user's
    // id_token and profile
    function logout() {
      sessionStorage.removeItem('id_token');
      sessionStorage.removeItem('profile');
      $state.go('home');
      authManager.unauthenticate();
    }

    // Set up the logic for when a user authenticates
    // This method is called from app.run.js
    function registerAuthenticationListener() {
      lock.on('authenticated', function (authResult, profile) {
        lock.getProfile(authResult.idToken, function(error, profile) {
            if (error) {
              // Handle error
              return;
            }
            if(profile.user_metadata.roles[0]== 'admin') {
              sessionStorage.setItem('idToken', authResult.idToken);
              sessionStorage.setItem('profile', JSON.stringify(profile));
              $state.go('dash');
            }
          });

        authManager.authenticate();
      });
    }

    return {
      login: login,
      logout: logout,
      registerAuthenticationListener: registerAuthenticationListener
    }
  }
})();

(function () {

  'use strict';

  angular
    .module('app')
    .controller('BannerController', BannerController);

  BannerController.$inject = ['authService', '$state', '$http', '$scope'];

  function BannerController(authService, $state, $http, $scope) {

    $('.modal').modal(
      {
        dismissible: false

      }
    );
    $('.collapsible').collapsible();
    $('select').material_select();

    var vm = this;
    vm.authService = authService;
    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        vm.authService.logout();
        $state.go('home');
      }
    }
    else{
      vm.authService.logout();
      $state.go('home');
    }

    function load() {
      $http.get("api/banners").then(function (res) {
        vm.banners = res.data;
        $scope.banners = res.data;
      });
    }
    load();

    $scope.banner = {};

    vm.upload = function(file){
      var reader = new window.FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        var base64data = reader.result;
        $scope.banner.foto = base64data;
      }
    };

    vm.eliminar = function(id){
      $http.delete("api/banners/"+id);
      load();
    };

    vm.nuevoBanner = function(banner){
      banner.id_banner = 0;
      $http.post("api/banners", banner).then(function (res) {
        load();
        $scope.banner = {};
      });
    };

  }

}());

(function () {
  'use strict';

  angular
    .module('app')
    .controller('DashController', DashController);

  DashController.$inject = ['authService', '$location', '$http', '$scope'];

  function DashController(authService, $location, $http, $scope) {

    $('.collapsible').collapsible();

    var vm = this;
    vm.authService = authService;
    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        vm.authService.logout();
        $state.go('home');
      }
    }
    else{
      vm.authService.logout();
      $state.go('home');
    }

    function load() {
      $http.get("api/restaurantes").then(function (res) {
        vm.restaurantes = res.data;
        $scope.restaurantes = res.data;
        console.log("restaurantes", res);
      });
    }

    load();

    vm.publicarResena = function (id) {
      $http.put("api/restaurantes/"+id, {estado: 'publicada'}).then(function (res) {
        load();
      });
    };

    vm.guardarReseña = function (id) {
      $http.put("api/restaurantes/"+id, {estado: 'guardada'}).then(function (res) {
        load();
      });
    };

    vm.ocultarResena = function (id) {
      $http.put("api/restaurantes/"+id, {estado: 'oculta'}).then(function (res) {
        load();
      });
    };

    vm.eliminar = function (id) {
      $http.delete("api/restaurantes/"+id).then(function (res) {
        load();
      });
    };

  }
}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['authService', '$state'];

  function HomeController(authService, $state) {

    var vm = this;
    vm.authService = authService;
    if(sessionStorage.getItem('profile')){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo == 'admin'){
        $state.go('dash');
      }
    }
  }

}());

(function () {
  'use strict';

  angular
    .module('app')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['authService'];

  function LoginController(authService) {

    var vm = this;
    vm.authService = authService;
  }

}());

(function () {
  'use strict';

  angular
    .module('app')
    .controller('NewController', NewController);

  NewController.$inject = ['authService', '$location', '$stateParams', '$http', '$scope', '$state'];

  function NewController(authService, $location, $stateParams, $http, $scope, $state) {


    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        authService.logout();
        $state.go('home');
      }
    }
    else{
      authService.logout();
      $state.go('home');
    }

    var date = new Date();

    date.getTime()

    $('.collapsible').collapsible();
    $('#mydiv').hide();
    var vm = this;
    vm.restaurante = {fotos:[], platos:[]};
    vm.authService = authService;
    vm.editando = false;
    if($stateParams.id){
      vm.editando = true;
      $http.get("api/restaurantes/"+$stateParams.id).then(function (res) {
        vm.restaurante = res.data;
        _.each(vm.tiposCom, function(com){
          _.each(vm.restaurante.tipoComida, function (tipoCom) {
            if(com.nombre == tipoCom){
              com.selected = true;
            }
          })
        });
        _.each(vm.tiposOca, function(oca){
          _.each(vm.restaurante.tipoOcasion, function (tipoOca) {
            if(oca.nombre == tipoOca){
              oca.selected = true;
            }
          })
        });
      });
    }
    vm.position = {};
    var userLatLng;
    var myOptions = {
      zoom : 16,
      center : userLatLng,
      mapTypeId : google.maps.MapTypeId.ROADMAP
    };


    var mapObject;

    navigator.geolocation.getCurrentPosition(locationSuccess, locationFail);


    function locationFail() {
      alert("Oops, could not find you.");
    }
    var rImg = "../../../../img/BE_Comidas";
    var ocaImg = "../../../../img/BE_Ocaciones";

    vm.tiposCom = [
      {nombre: "Americana", img: rImg+"/BE_ComidasBlack-01.png", imgSel: rImg+"/BE_ComidasWhite-01.png", selected: false},
      {nombre: "Asiatica", img: rImg+"/BE_ComidasBlack-02.png", imgSel: rImg+"/BE_ComidasWhite-02.png", selected: false},
      {nombre: "Cafe", img: rImg+"/BE_ComidasBlack-03.png", imgSel: rImg+"/BE_ComidasWhite-03.png", selected: false},
      {nombre: "Carne", img: rImg+"/BE_ComidasBlack-04.png", imgSel: rImg+"/BE_ComidasWhite-04.png", selected: false},
      {nombre: "Rapida", img: rImg+"/BE_ComidasBlack-05.png", imgSel: rImg+"/BE_ComidasWhite-05.png", selected: false},
      {nombre: "Española", img: rImg+"/BE_ComidasBlack-06.png", imgSel: rImg+"/BE_ComidasWhite-06.png", selected: false},
      {nombre: "Francesa", img: rImg+"/BE_ComidasBlack-07.png", imgSel: rImg+"/BE_ComidasWhite-07.png", selected: false},
      {nombre: "Hamburguesas", img: rImg+"/BE_ComidasBlack-08.png", imgSel: rImg+"/BE_ComidasWhite-08.png", selected: false},
      {nombre: "Italiana", img: rImg+"/BE_ComidasBlack-09.png", imgSel: rImg+"/BE_ComidasWhite-09.png", selected: false},
      {nombre: "Mediterranea", img: rImg+"/BE_ComidasBlack-10.png", imgSel: rImg+"/BE_ComidasWhite-10.png", selected: false},
      {nombre: "Mexicana", img: rImg+"/BE_ComidasBlack-11.png", imgSel: rImg+"/BE_ComidasWhite-11.png", selected: false},
      {nombre: "Peruana", img: rImg+"/BE_ComidasBlack-12.png", imgSel: rImg+"/BE_ComidasWhite-12.png", selected: false},
      {nombre: "Pizza", img: rImg+"/BE_ComidasBlack-13.png", imgSel: rImg+"/BE_ComidasWhite-13.png", selected: false},
      {nombre: "Saludable", img: rImg+"/BE_ComidasBlack-14.png", imgSel: rImg+"/BE_ComidasWhite-14.png", selected: false},
      {nombre: "Sanduche", img: rImg+"/BE_ComidasBlack-15.png", imgSel: rImg+"/BE_ComidasWhite-15.png", selected: false},
      {nombre: "Sushi", img: rImg+"/BE_ComidasBlack-16.png", imgSel: rImg+"/BE_ComidasWhite-16.png", selected: false},
      {nombre: "No Convencional", img: rImg+"/BE_ComidasBlack-17.png", imgSel: rImg+"/BE_ComidasWhite-17.png", selected: false},
      {nombre: "Fusión", img: rImg+"/BE_ComidasBlack-18.png", imgSel: rImg+"/BE_ComidasWhite-18.png", selected: false},
      {nombre: "Mar", img: rImg+"/BE_ComidasBlack-19.png", imgSel: rImg+"/BE_ComidasWhite-19.png", selected: false},
      {nombre: "Colombiana", img: rImg+"/BE_ComidasBlack-20.png", imgSel: rImg+"/BE_ComidasWhite-20.png", selected: false}
    ];

    vm.tiposOca = [
      {nombre: "Alta cocina", img: ocaImg+"/BE_Ocaciones_Black-01.png", imgSel: ocaImg+"/BE_Ocaciones_White-01.png", selected: false},
      {nombre: "Buena Terraza", img: ocaImg+"/BE_Ocaciones_Black-02.png", imgSel: ocaImg+"/BE_Ocaciones_White-02.png",  selected: false},
      {nombre: "En los rines", img: ocaImg+"/BE_Ocaciones_Black-03.png", imgSel: ocaImg+"/BE_Ocaciones_White-03.png",  selected: false},
      {nombre: "Desayunos", img: ocaImg+"/BE_Ocaciones_Black-04.png", imgSel: ocaImg+"/BE_Ocaciones_White-04.png",  selected: false},
      {nombre: "Cumpleaños", img: ocaImg+"/BE_Ocaciones_Black-05.png", imgSel: ocaImg+"/BE_Ocaciones_White-05.png",  selected: false},
      {nombre: "Domingo Abuela", img: ocaImg+"/BE_Ocaciones_Black-06.png", imgSel: ocaImg+"/BE_Ocaciones_White-06.png",  selected: false},
      {nombre: "De mes", img: ocaImg+"/BE_Ocaciones_Black-07.png", imgSel: ocaImg+"/BE_Ocaciones_White-07.png",  selected: false},
      {nombre: "Para el gringo", img: ocaImg+"/BE_Ocaciones_Black-08.png", imgSel: ocaImg+"/BE_Ocaciones_White-08.png",  selected: false},
      {nombre: "Primera cita", img: ocaImg+"/BE_Ocaciones_Black-09.png", imgSel: ocaImg+"/BE_Ocaciones_White-09.png",  selected: false},
      {nombre: "Rapida", img: ocaImg+"/BE_Ocaciones_Black-10.png", imgSel: ocaImg+"/BE_Ocaciones_White-10.png",  selected: false},
      {nombre: "Grado", img: ocaImg+"/BE_Ocaciones_Black-11.png", imgSel: ocaImg+"/BE_Ocaciones_White-11.png",  selected: false},
      {nombre: "Clandestino", img: ocaImg+"/BE_Ocaciones_Black-12.png", imgSel: ocaImg+"/BE_Ocaciones_White-12.png",  selected: false},
      {nombre: "Tarjeta corporativa", img: ocaImg+"/BE_Ocaciones_Black-13.png", imgSel: ocaImg+"/BE_Ocaciones_White-13.png",  selected: false},
      {nombre: "Trago y picadas", img: ocaImg+"/BE_Ocaciones_Black-14.png", imgSel: ocaImg+"/BE_Ocaciones_White-14.png",  selected: false},
      {nombre: "Precopeo", img: ocaImg+"/BE_Ocaciones_Black-15.png", imgSel: ocaImg+"/BE_Ocaciones_White-15.png",  selected: false},
      {nombre: "Oficinero", img: ocaImg+"/BE_Ocaciones_Black-16.png", imgSel: ocaImg+"/BE_Ocaciones_White-16.png",  selected: false}
    ];

    function locationSuccess(position) {
      userLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      myOptions.center = userLatLng;
      mapObject = new google.maps.Map(document.getElementById("map"), myOptions);
      google.maps.event.addListener(mapObject, 'click', function(event) {
        addMarker(event.latLng, mapObject);
      });
      if($stateParams.id){
        addInitialMarker(vm.restaurante.geoloc, mapObject);
      }
    }
    var marker;
    function addMarker(location, map) {
      vm.restaurante.geoloc = {lat: location.lat(), lng: location.lng()};
      if ( marker ) {
        marker.setPosition(location);
      } else {
        marker = new google.maps.Marker({
          position: location,
          map: map
        });
      }
    }

    function addInitialMarker(geoloc, map) {
      console.log("set");
      var latlng = new google.maps.LatLng(geoloc.lat, geoloc.lng);
      marker = new google.maps.Marker({
        position: latlng,
        map: map
      });
    }

    vm.upload = function(file){
      console.log(file);
      var reader = new window.FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        var base64data = reader.result;
        vm.restaurante.fotos.push(base64data);
      }
    };

    vm.guardar = function (restaurante) {
      $('#mydiv').show();
      restaurante.estado = 'guardada';
      restaurante.fecha = new Date();
      restaurante.calUsers = [];
      var tiposOc = _.map(_.groupBy(vm.tiposOca, 'selected').true, function (oca) {
        return oca.nombre;
      });
      var tiposCo = _.map(_.groupBy(vm.tiposCom, 'selected').true, function (com) {
        return com.nombre;
      });
      restaurante.tipoComida = tiposCo;
      restaurante.tipoOcasion = tiposOc;
      $http.post("api/restaurantes", restaurante).then(function (res) {
        $state.go('dash');
        $('#mydiv').hide();
      }).catch(function (err) {
        var mensaje = "Favor completar todos lo campos: ";
        if(restaurante.nombre == null){
          mensaje = mensaje + " nombre";
        }
        if(restaurante.zona == null){
          mensaje = mensaje + " zona";
        }
        if(restaurante.geoloc == null){
          mensaje = mensaje + " ubicación";
        }
        if(restaurante.tipoComida.length == 0){
          mensaje = mensaje + " tipo de comida";
        }
        if(restaurante.tipoOcasion.length == 0){
          mensaje = mensaje + " tipo de ocasion";
        }
        $('#mydiv').hide();
        alert(mensaje);
      })
    };

    vm.publicar = function (restaurante) {
      $('#mydiv').show();
      restaurante.estado = 'publicada';
      restaurante.fecha = new Date();
      restaurante.calUsers = [];
      var tiposOc = _.map(_.groupBy(vm.tiposOca, 'selected').true, function (oca) {
        return oca.nombre;
      });
      var tiposCo = _.map(_.groupBy(vm.tiposCom, 'selected').true, function (com) {
        return com.nombre;
      });
      restaurante.tipoComida = tiposCo;
      restaurante.tipoOcasion = tiposOc;
      $http.post("api/restaurantes", restaurante).then(function (res) {
        $state.go('dash');
        $('#mydiv').hide();
      }).catch(function (err) {
        var mensaje = "Favor completar todos lo campos: ";
        if(restaurante.nombre == null){
          mensaje = mensaje + " nombre";
        }
        if(restaurante.zona == null){
          mensaje = mensaje + " zona";
        }
        if(restaurante.geoloc == null){
          mensaje = mensaje + " ubicación";
        }
        if(restaurante.tipoComida.length == 0){
          mensaje = mensaje + " tipo de comida";
        }
        if(restaurante.tipoOcasion.length == 0){
          mensaje = mensaje + " tipo de ocasion";
        }
        $('#mydiv').hide();
        alert(mensaje);
      })
    };

    vm.publicarNuevo = function (restaurante) {
      $('#mydiv').show();
      restaurante.fecha = new Date();
      var tiposOc = _.map(_.groupBy(vm.tiposOca, 'selected').true, function (oca) {
        return oca.nombre;
      });
      var tiposCo = _.map(_.groupBy(vm.tiposCom, 'selected').true, function (com) {
        return com.nombre;
      });
      restaurante.tipoComida = tiposCo;
      restaurante.tipoOcasion = tiposOc;
      $http.put("api/restaurantes", restaurante).then(function (res) {
        $state.go('dash');
        $('#mydiv').hide();
      }).catch(function (err) {
        var mensaje = "Favor completar todos lo campos: ";
        if(restaurante.nombre == null){
          mensaje = mensaje + " nombre";
        }
        if(restaurante.zona == null){
          mensaje = mensaje + " zona";
        }
        if(restaurante.geoloc == null){
          mensaje = mensaje + " ubicación";
        }
        if(restaurante.tipoComida.length == 0){
          mensaje = mensaje + " tipo de comida";
        }
        if(restaurante.tipoOcasion.length == 0){
          mensaje = mensaje + " tipo de ocasion";
        }
        $('#mydiv').hide();
        console.log(err);
        alert(mensaje + err);
      })
    }

    vm.removeFoto = function(index){
      vm.restaurante.fotos.splice(index, 1);
    }

  }

}());

(function () {
  'use strict';

  angular
    .module('app')
    .controller('TerminosController', TerminosController);

  TerminosController.$inject = ['authService', '$location', '$stateParams', '$http', '$scope', '$state'];

  function TerminosController(authService, $location, $stateParams, $http, $scope, $state) {


    if(JSON.parse(sessionStorage.getItem('profile')).user_metadata){
      var tipo = JSON.parse(sessionStorage.getItem('profile')).user_metadata.roles[0];
      if(tipo != 'admin'){
        authService.logout();
        $state.go('home');
      }
    }
    else{
      authService.logout();
      $state.go('home');
    }

    function loadTerminos() {
      var source = "/api/terminos";
      $http.get(source).then(function (res) {
        $scope.termino = res.data[0];
      })
    }

    loadTerminos();

    $scope.updateTerminos = function(termino) {
      var source = "/api/terminos";
      $http.put(source, termino).then(function (res) {
        $state.go('dash');
      });
    }

  }

}());
